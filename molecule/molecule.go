package molecule

import (
    // "fmt"
    "strings"

    "ecrs.ru/goterm/substance"
)

// Molecule is a slice of atoms
type Molecule []substance.Substancer

// New return new molecule from
// slice of Substancer
func New(subst ...substance.Substancer) *Molecule {
    m := Molecule(subst)
    return &m
}

// Unknown return unknowk molecule
func Unknown() *Molecule {
    s := substance.New(substance.Unknown, substance.Prnt, substance.Unknown.Repr())
    return New(s)
}

// String return molecule representation
func (m *Molecule) String() string {
    return strings.Join(m.Atsos(), " ")
}

// Car return pointer to new Molecule
// from current molecule.
// If len of current molecule == 0
// return new empty Molecule
func (m *Molecule) Car() *Molecule {
    if len(*m) > 0 {
        out := (*m)[:1]
        return &out
    }
    return New()
}

// Cdr return pointer to Molecule
// from current molecule
// If len of current molecule < 2
// return new empty Molecule
func (m *Molecule) Cdr() *Molecule {
    if len(*m) > 1 {
        out := (*m)[1:]
        return &out
    }
    return New()
}

// // Lsh - left shift (like clockwise rotation)
// // return new molecule where first atom moved
// // to the end of current molecule
// // Otherwise returns current molecule
func (m *Molecule) Lsh() *Molecule {
    if len(*m) > 1 {
        res := make(Molecule, 0, len(*m))
        res = append(res, (*m)[1:]...)
        res = append(res, (*m)[0])
        return &res
    }
    return m
}

// Rsh - right shift (like counterclockwise rotation)
// return new molecule where last atom moved
// to the begining of current molecule
// Otherwise returns current molecule
func (m *Molecule) Rsh() *Molecule {
    if len(*m) > 1 {
        res := make(Molecule, 0, len(*m))
        res = append(res, (*m)[len(*m) - 1])
        res = append(res, (*m)[:len(*m) - 1]...)
        return &res
    }
    return m
}

func (m *Molecule) Atsos() []string {
    out := make([]string, 0)
    for _, a := range *m {
        out = append(out, string(a.Repr()))
    }
    return out
}

// // Len return len of atoms
// func (m *Molecule) Len() int {
//     return len(m.atoms)
// }

func (m *Molecule) Knd() *Molecule {
    s := substance.New(substance.Symbol, substance.Prnt, m.Kind().Repr())
    return New(s)
}

// Kind return kind of the first atom
func (m *Molecule) Kind() substance.Kind {
    if len(*m) > 0 {
        return (*m)[0].Kind()
    }
    return substance.Kind(0)
}

// Actn return action of the first atom
func (m *Molecule) Actn() substance.Action {
    if len(*m) > 0 {
        return (*m)[0].Actn()
    }
    return substance.Action(0)
}

// func Mut
// len of molecule must be >= 3
// first atom - kind
// second atom - action
// third atom - repr
// return new molecule with the first three
// atoms replaces by a new one
func (m *Molecule) Mut() *Molecule {
    if len(*m) > 2 {
        a := substance.New((*m)[0].Kind(), (*m)[1].Actn(), (*m)[2].Repr())
        res := make(Molecule, 0, len(*m)-3)
        res = append(res, a)
        res = append(res, (*m)[3:]...)
        return &res
    }
    return Unknown()
}

// func Mol
// len of molecule must be >= 2
// first atom - len of new molecule (must be Number kind)
// rest n atoms - new molecule (n == value of the first atom)
// marks atoms as a single molecule and return it
func (m *Molecule) Mol() *Molecule {
    if len(*m) > 1 {
        a := (*m)[0]
        if a.Kind() != substance.Number {
            return Unknown()
        } else {
            l := a.AsNumber()
            if len(*m) > l {
            res := make(Molecule, 0, l)
            res = append(res, (*m)[1:l + 1]...)
                return &res
            } else {
                return Unknown()
            }
        }
    }
    return Unknown()
}

// func Sum
// return sum of Number atoms
func (m *Molecule) Sum() *Molecule {
    if len(*m) > 0 {
        s := 0 // returning sum
        i := 0 // index of atom in current molecule
        tmp := substance.New(substance.Symbol, substance.Prnt, "")
        for i < (len(*m)) && (*m)[i].Kind() == substance.Number {
            s += (*m)[i].AsNumber()
            i++
        }
        res := substance.New(substance.Symbol, substance.Prnt, tmp.Repr(s))
        return New(res)
    }
    return Unknown()
}
