package molecule

import (
	"fmt"
	"testing"
	"strings"

	"ecrs.ru/goterm/atom"
	"ecrs.ru/goterm/substance"
)

type TestStruct struct {
	name string
	input string
	expected string
}



func TestMolecule(t *testing.T) {
    tests := []TestStruct{
		{"Molecule: 1 2 3", "1 2 3", "1 2 3"},
		{"Molecule: car 1 2 3", "car 1 2 3", "CAR 1 2 3"},
		{"Molecule: cdr 1 2 3", "cdr 1 2 3", "CDR 1 2 3"},
		{"Molecule: hlt", "hlt", "HLT"},
		{"Molecule: cdr 1 hlt 2 3 4", "cdr 1 hlt 2 3 4", "CDR 1 HLT 2 3 4"},
		{"Molecule: ", "", ""},
    }

    for _, v := range tests {
        tst := New(atom.Atomizer(strings.Fields(v.input))...)
        got := fmt.Sprintf("%s", tst)
        exp := fmt.Sprintf("%s", v.expected)
        if exp != got {
           t.Errorf("incorrect molecule: %s\nexpected: %s, got: %s\n", v.name, exp, got)
        }    
    }
}

func TestCar(t *testing.T) {
    tests := []TestStruct{
		{"Molecule: 1 2 3", "1 2 3", "1"},
		{"Molecule: car 1 2 3", "car 1 2 3", "CAR"},
		{"Molecule: cdr 1 2 3", "cdr 1 2 3", "CDR"},
		{"Molecule: hlt", "hlt", "HLT"},
		{"Molecule: cdr 1 hlt 2 3 4", "cdr 1 hlt 2 3 4", "CDR"},
		{"Molecule: ", "", ""},
    }

    for _, v := range tests {
        tst := New(atom.Atomizer(strings.Fields(v.input))...)
        got := fmt.Sprintf("%s", tst.Car())
        exp := fmt.Sprintf("%s", v.expected)
        if exp != got {
           t.Errorf("incorrect car: %s\nexpected: %s, got: %s\n", v.name, exp, got)
        }    
    }
}

func TestCdr(t *testing.T) {
    tests := []TestStruct{
		{"Molecule: 1 2 3", "1 2 3", "2 3"},
		{"Molecule: car 1 2 3", "car 1 2 3", "1 2 3"},
		{"Molecule: cdr 1 2 3", "cdr 1 2 3", "1 2 3"},
		{"Molecule: hlt", "hlt", ""},
		{"Molecule: cdr 1 hlt 2 3 4", "cdr 1 hlt 2 3 4", "1 HLT 2 3 4"},
		{"Molecule: ", "", ""},
    }

    for _, v := range tests {
        tst := New(atom.Atomizer(strings.Fields(v.input))...)
        got := fmt.Sprintf("%s", tst.Cdr())
        exp := fmt.Sprintf("%s", v.expected)
        if exp != got {
           t.Errorf("incorrect cdr: %s\nexpected: %s, got: %s\n", v.name, exp, got)
        }    
    }
}

func TestLsh(t *testing.T) {
    tests := []TestStruct{
		{"Molecule: 1 2 3", "1 2 3", "2 3 1"},
		{"Molecule: car 1 2 3", "car 1 2 3", "1 2 3 CAR"},
		{"Molecule: cdr 1 2 3", "cdr 1 2 3", "1 2 3 CDR"},
		{"Molecule: hlt", "hlt", "HLT"},
		{"Molecule: cdr 1 hlt 2 3 4", "cdr 1 hlt 2 3 4", "1 HLT 2 3 4 CDR"},
		{"Molecule: ", "", ""},
    }

    for _, v := range tests {
        tst := New(atom.Atomizer(strings.Fields(v.input))...)
        got := fmt.Sprintf("%s", tst.Lsh())
        exp := fmt.Sprintf("%s", v.expected)
        if exp != got {
           t.Errorf("incorrect lsh: %s\nexpected: %s, got: %s\n", v.name, exp, got)
        }    
    }
}

func TestRsh(t *testing.T) {
    tests := []TestStruct{
		{"Molecule: 1 2 3", "1 2 3", "3 1 2"},
		{"Molecule: car 1 2 3", "car 1 2 3", "3 CAR 1 2"},
		{"Molecule: cdr 1 2 3", "cdr 1 2 3", "3 CDR 1 2"},
		{"Molecule: hlt", "hlt", "HLT"},
		{"Molecule: cdr 1 hlt 2 3 4", "cdr 1 hlt 2 3 4", "4 CDR 1 HLT 2 3"},
		{"Molecule: ", "", ""},
    }

    for _, v := range tests {
        tst := New(atom.Atomizer(strings.Fields(v.input))...)
        got := fmt.Sprintf("%s", tst.Rsh())
        exp := fmt.Sprintf("%s", v.expected)
        if exp != got {
           t.Errorf("incorrect rsh: %s\nexpected: %s, got: %s\n", v.name, exp, got)
        }    
    }
}

// func TestLen(t *testing.T) {
//     tests := []TestStruct{
// 		{"Molecule: 1 2 3", "1 2 3", "3"},
// 		{"Molecule: car 1 2 3", "car 1 2 3", "4"},
// 		{"Molecule: cdr 1 2 3", "cdr 1 2 3", "4"},
// 		{"Molecule: hlt", "hlt", "1"},
// 		{"Molecule: cdr 1 hlt 2 3 4", "cdr 1 hlt 2 3 4", "6"},
// 		{"Molecule: ", "", "0"},
//     }

//     for _, v := range tests {
//         tst := New(atom.Atomizer(strings.Fields(v.input))...)
//         got := fmt.Sprintf("%d", tst.Len())
//         exp := fmt.Sprintf("%s", v.expected)
//         if exp != got {
//            t.Errorf("incorrect len: %s\nexpected: %s, got: %s\n", v.name, exp, got)
//         }    
//     }
// }

func TestKind(t *testing.T) {
    tests := []TestStruct{
		{"Molecule: 1 2 3", "1 2 3", fmt.Sprintf("%v", substance.Number)},
		{"Molecule: car 1 2 3", "car 1 2 3", fmt.Sprintf("%v", substance.Command)},
		{"Molecule: cdr 1 2 3", "cdr 1 2 3", fmt.Sprintf("%v", substance.Command)},
		{"Molecule: hlt", "hlt", fmt.Sprintf("%v", substance.Command)},
		{"Molecule: cdr 1 hlt 2 3 4", "cdr 1 hlt 2 3 4", fmt.Sprintf("%v", substance.Command)},
		{"Molecule: ", "", fmt.Sprintf("%v", substance.Unknown)},
    }

    for _, v := range tests {
        tst := New(atom.Atomizer(strings.Fields(v.input))...)
        got := fmt.Sprintf("%d", tst.Kind())
        exp := fmt.Sprintf("%s", v.expected)
        if exp != got {
           t.Errorf("incorrect kind: %s\nexpected: %s, got: %s\n", v.name, exp, got)
        }    
    }
}
