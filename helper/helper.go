package helper

import (
	"fmt"
	"reflect"
)

type TypeFuncStruct struct {
	Type string
	NumIn map[int]reflect.Type
	NumOut map[int]reflect.Type
}

func New(t string, i, o map[int]reflect.Type) TypeFuncStruct {
	return TypeFuncStruct{
		Type: t,
		NumIn: i,
		NumOut: o,
	}
}

func (tfs TypeFuncStruct) IsValid() bool {
	return tfs.Type != ""
}

func TypeInfo(name string, iface interface{}) TypeFuncStruct {
	fmt.Printf("=== %s ===\n", name)
	i_type := reflect.TypeOf(iface)
	i_value := reflect.ValueOf(iface)

	fmt.Printf("Func type: %v\nFunc value: %v\n", i_type, i_value)
	fmt.Printf("Type align: %v\n", i_type.Align())
	fmt.Printf("Type field align: %v\n", i_type.FieldAlign())
	fmt.Printf("Type name: %v\n", i_type.Name())
	fmt.Printf("Type num method: %v\n", i_type.NumMethod())
	fmt.Printf("Type pkg path: %v\n", i_type.PkgPath())
	fmt.Printf("Type size: %v\n", i_type.Size())
	fmt.Printf("Type string: %v\n", i_type.String())
	i_kind := i_type.Kind()
	fmt.Printf("Type kind: %v\n", i_kind)
	switch {
	case i_kind == reflect.Func:
		fmt.Printf("Type comparable: %v\n", i_type.Comparable())
		fmt.Printf("Type is variadic: %v\n", i_type.IsVariadic()) // panic for []int{}
		num_in := i_type.NumIn()
		in_map := make(map[int]reflect.Type, num_in)
		fmt.Printf("Type num in: %v\n", num_in)
		for i := range UpTo(num_in) {
			fmt.Printf("  Type in %d: %v\n", i, i_type.In(i))
			in_map[i] = i_type.In(i)
		}
		num_out := i_type.NumOut()
		out_map := make(map[int]reflect.Type, num_out)
		fmt.Printf("Type num out: %v\n", num_out)
		for i := range UpTo(num_out) {
			fmt.Printf("  Type out %d: %v\n", i, i_type.Out(i))
			out_map[i] = i_type.Out(i)
		}
		return New(i_type.String(), in_map, out_map)
	case i_kind == reflect.Int:
		fmt.Printf("Type bits: %v\n", i_type.Bits()) // panic for func, slice
		// fmt.Printf("Type elem: %v\n", i_type.Elem()) // panic for func, slice
	case i_kind == reflect.Array:
		fmt.Printf("Type len: %v\n", i_type.Len()) // panic for func, slice
	}
	fmt.Println()
	return TypeFuncStruct{}
}

func UpTo(max int) []struct{} {
	// https://github.com/jonbodner/ranger
	if max < 0 {
		return nil
	}
	return make([]struct{}, max)
}
