package types

import (
	"fmt"
	"os"
	"reflect"

	// "ecrs.ru/goterm/helper"
)

type Command struct {
	meth interface{}
	desc string
	params []interface{}
}

type CommandPool map[string]Command


func NewCommandPool() CommandPool {
	return CommandPool{
		// "": Command{
		// 	meth: nil,
		// 	desc: "Go Terminal v0.0.1",
		// 	},
		"exit": Command{
			meth: os.Exit,
			desc: "Exit from go terminal",
			params: append(make([]interface{}, 0), 0),
		},
	}
}

func (c CommandPool) RegisterCommand(name string, meth interface{}, desc string, params ...interface{}) {
	c[name] = Command{
		meth: meth,
		desc: desc,
		params: params,
	}
}

func (c CommandPool) ShowAllCommadns() {
	for command := range c {
		fmt.Printf("%s: %s\n", command, c[command].desc)
	}
}

func (c CommandPool) Exists(name string) bool {
	if _, ok := c[name]; ok {
		return true
	}

	return false
}

func (c CommandPool) FuncInfo(name string) {
	f := c[name].meth
	f_type := reflect.TypeOf(f)
	f_val := reflect.ValueOf(f)
	fmt.Printf("FuncInfo: M:%v T:%v V:%v K:%v\n", f, f_type, f_val, f_type.Kind())
	fmt.Printf("FuncInfo: NI:%v\n", f_type.NumIn())
	for ni := 0; ni < f_type.NumIn(); ni++ {
		fmt.Printf("=> %v\n", f_type.In(ni).Kind())
	}
}

// func (c CommandPool) FunctionParamBuilder(ui UserInput) []reflect.Value {
// 	// ui_car := ui.Car()
// 	// ui_cdr := ui.Cdr()
// 	// fmt.Printf("RT:%v RV:%v RK:%v\n", r_type, r_val, r_type.Kind())
// 	p := c[ui.car].params
// 	fmt.Printf("%v L:%d C:%d\n", p, len(p), cap(p))
// 	params := make([]reflect.Value, 0)
// 	if len(ui.cdr) > 0 {
// 		for _, param := range ui.cdr {
// 			params = append(params, reflect.ValueOf(param).Convert(reflect.TypeOf(c[ui.car].meth).Out(0)))
// 		}
// 	} else {
// 		for _, param := range p {
// 			params = append(params, reflect.ValueOf(param))
// 		}
// 	}
// 	return params
// }

// func (c CommandPool) FunctionDefaultParamBuilder(tfs helper.TypeFuncStruct) []reflect.Value {
// 	params := make([]reflect.Value, 0)
// 	for _, v := range tfs.NumIn {
// 		params = append(params, reflect.ValueOf(v))
// 	}
// 	return params
// }

// func (c CommandPool) Exec(ui UserInput) {
// 	// // tfs for registered command linking by name ui.car
// 	// tfs := helper.TypeInfo(ui.Car(), c[ui.Car()].meth)
// 	// if tfs.IsValid() {
// 	// 	fmt.Printf("TypeFuncInfo: %v\n", tfs)
// 	// }
// 	// f_val := reflect.ValueOf(c[ui.Car()].meth)
// 	// if len(tfs.NumIn) != len(ui.Cdr()) {
// 	// 	f_val.Call(c.FunctionParamBuilder(ui))
// 	// } else {
// 	// 	f_val.Call(c.FunctionDefaultParamBuilder(tfs))
// 	// }
// }

// func (c CommandPool) GetCommand(ui *UserInput) interface{} {
// 	return c[ui.Car()].meth
// }



// golang default structs
// https://stackoverflow.com/questions/37135193/how-to-set-default-values-in-go-structs

// type GT_Settings struct {
// 	// version
// 	Major int `default:"0"`
// 	Minor int `default:"0"`
// 	Patch int `default:"1"`
// 	// build
// 	Build string `default:"debug"`
// }

// func setField(field reflect.Value, defaultVal string) error {

// 	if !field.CanSet() {
// 		return fmt.Errorf("Can't set value\n")
// 	}

// 	switch field.Kind() {

// 	case reflect.Int:
// 		if val, err := strconv.ParseInt(defaultVal, 10, 64); err == nil {
// 			field.Set(reflect.ValueOf(int(val)).Convert(field.Type()))
// 		}
// 	case reflect.String:
// 		field.Set(reflect.ValueOf(defaultVal).Convert(field.Type()))
// 	}

// 	return nil
// }

// func Set(ptr interface{}, tag string) error {
// 	if reflect.TypeOf(ptr).Kind() != reflect.Ptr {
// 		return fmt.Errorf("Not a pointer")
// 	}

// 	v := reflect.ValueOf(ptr).Elem()
// 	t := v.Type()

// 	for i := 0; i < t.NumField(); i++ {
// 		if defaultVal := t.Field(i).Tag.Get(tag); defaultVal != "-" {
// 			if err := setField(v.Field(i), defaultVal); err != nil {
// 				return err
// 			}

// 		}
// 	}
// 	return nil
// }
