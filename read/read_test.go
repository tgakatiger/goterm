package read

import (
       "fmt"
       "testing"
)

func TestNew(t *testing.T) {
    tests := map[string]string{
          "1 2 3": "1 2 3",
          "car 1 2 3": "car 1 2 3",
          "cdr 1 2 3": "cdr 1 2 3",
    }

    for k, v := range tests {
        res := New(k)
        a := fmt.Sprintf("%v", res.Atsos())
        b := fmt.Sprintf("[%s]", v)
        if a != b {
           t.Errorf("incorrect new: %s\nexpected: %s, got: %s\n", k, a, b)
        }    
    }


	
}
