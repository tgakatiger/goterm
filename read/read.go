package read

import (
    "fmt"
    "strings"
    "bufio"
    "os"
)

// type UserInput
// it's a raw normalized slice of strings
// normalization means all strings are not
// empty and without whitespaces
type UserInput struct {
    raw []string
}


func (ui *UserInput) String() string {
    return strings.Join(ui.raw, " ")
}

// New func return pointer to UserInput srtruct
//
// user input splitted by whitespaces and lowercased
func New(s string) *UserInput {
    r := strings.Fields(strings.ToLower(s))
    return &UserInput{
        raw: r,
    }
}

// Prompt func ask user for input text
// and return pointer to new UserInput
//
// you can self define prompt (only first arg used)
// default prompt is `>>> `
func Prompt(prompt ...string) *UserInput {
    scanner := bufio.NewScanner(os.Stdin)
    if len(prompt) > 0 {
        fmt.Printf("%s", prompt[0])
    } else {
        fmt.Printf(">>> ")
    }
    scanner.Scan()
    return New(scanner.Text())
}

// Atsos - at slice of strings
// на срезе струн © translate.google.com
func (ui *UserInput) Atsos() []string {
     return ui.raw
}
