package eval

import (
    "fmt"

    "ecrs.ru/goterm/molecule"
    "ecrs.ru/goterm/substance"
)

// пока что вывод здесь
// но потом надо будет перенести его в print.Printer()
// а тут обработка логики молекул
func Evaluator(m molecule.Molecule) *molecule.Molecule {
    defer func() {
        fmt.Println()
    }()

    fmt.Printf("Molecule: %v\n", m)
    fmt.Printf("Evaluator: ")
    if m.Kind() != substance.Command {
        fmt.Printf("%v ", m)
    } else {
        switch m.Actn() {
        case substance.Car:
            return m.Cdr().Car()
        case substance.Cdr:
            return m.Cdr().Cdr()
        case substance.Lsh:
            return m.Cdr().Lsh()
        case substance.Rsh:
            return m.Cdr().Rsh()
        case substance.Knd:
            return m.Cdr().Knd()
        case substance.Mut:
            return m.Cdr().Mut()
        case substance.Sum:
            return m.Cdr().Sum()
        case substance.Mol:
            return m.Cdr().Mol()
        default:
            fmt.Printf("Command: %s defined but not implemented\n", (*m.Car())[0].Repr())
        }
    }
    return molecule.Unknown()
}

func EvaluationLoop(subst []molecule.Molecule) {
    new_subst := make([]molecule.Molecule, 0)
    printer := make([]molecule.Molecule, 0)
    for _, s := range subst {
        em := Evaluator(s)
        if em.Kind() != substance.Command {
            printer = append(printer, *em)
        } else {
            new_subst = append(new_subst, *em)
        }
    }

    if len(new_subst) > 0 {
        EvaluationLoop(new_subst)
    }

    for _, m := range printer {
        fmt.Printf("%v\n", m)
    }
}
