package substance

type Kind uint

// вид атома
// Неизвестный Команда Символ Число

const (
    Unknown Kind = iota
    Command
    Symbol
    Number
)

// ???
// func KindPool() map[string]Kind {
//     return map[string]Kind{
//         "COM": Command,
//         "SYM": Symbol,
//         "NUM": Number,
//     }
// }

// func Repr for Kind type
// return Kind as Repr (now it's a string)
func (k Kind) Repr() Repr {
    kinds := map[Kind]Repr{
        Command: "Command",
        Symbol: "Symbol",
        Number: "Number",
    }

    if _, ok := kinds[k]; ok {
        return kinds[k]
    }
    return "Unknown"
}
