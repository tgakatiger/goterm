package substance

type Action uint

// действия отвечают за поведение атома
// в зависимости от вида атома и его действия
// могут получаться разные результаты
// некоторые действия применимы к атомав всех видов
// и вызывюат пимерно одинаковое поведение
// например Prnt помечает атом для вывода на экран принтером,
// а Eval помечает атом для вычисления
//
// Number вычислается, как бы это ни было странно в число
// Symbol в зависимости от того связано с ним какое то значение или нет
// может вычислится либо в это значение, либо сам в себя
//
// Command вычисляется либо в команду связанную с символом, описывающим эту
// команду, либо в команду связанную с Action
// Вот пара примеров:
//
// car 1 2 3 - молекула состоящая из трех атомов, где
//             car - атом, вида Command (согласно определению в atom/command.go)
//                   с действием по умолчанию - Eval (см. func Atomizer)
//                   представляется (выводится на экран) как CAR
//             1   - атом, вида Number
//                   действие Prnt
//                   представление 1 (как строка "1" в других языках)
//             2,3 - то же самое, как и 1, только 2 и 3.//
//
// В результате вычисления данной молекулы получится вывод 1
//
// Но у атома car может быть изменены вид, действие и представление
// Например:
//
// car 1 2 3   далее приведена стрвуктура измененого атома car
//             Atom {
//                  kind: Command,
//                  actn: Cdr,
//                  repr: "?"
//                  }
// Вывод покажет 2 3, но если включена отладочная информация,
// то молекула будет отображена как ? 1 2 3
//
// Тут атом не менял вида, но мог бы

const (
    Eval Action = iota
    Prnt
    Hlt
    Car
    Cdr
    Lsh
    Rsh
    Sum
    Sub
    Mul
    Div
    Com
    Res
    Knd
    Act
    Rep
    Mut
	Mol
)
