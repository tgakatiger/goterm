package substance

import (
    "fmt"
    "strconv"
)

type Repr string

// Substancer is main interface
// implemented by Atom, Molecule
// and Substance (concrete struct)
type Substancer interface {
    Actn(...interface{}) Action
    Kind(...interface{}) Kind
    Repr(...interface{}) Repr
    String() string
    Decay() Substancer
    Mutation(...interface{}) Substancer
    Atsos() []string
    AsNumber() int
}

// struct Substance
type Substance struct {
    K Kind
    A Action
    R Repr
}

// func Actn
// default is getter for Substance.actn
// only first param are used if
// reimplemented
func (s Substance) Actn(a ...interface{}) Action {
    return s.A
}

func (s Substance) Kind(k ...interface{}) Kind {
    return s.K
}

func (s Substance) Repr(r ...interface{}) Repr {
    if r != nil && len(r) > 0 {
        switch t := r[0].(type) {
        case int:
            return Repr(strconv.Itoa(r[0].(int)))
        case string:
            return Repr(r[0].(string))
        default:
            fmt.Printf("Substance: can't convert %T to Repr\n", t)
        }
    }
    return s.R
}

func New(k Kind, a Action, r Repr) Substance {
    return Substance{
        K: k,
        A: a,
        R: r,
    }
}

func (s Substance) String() string {
    return fmt.Sprintf("(%d|%d|%s)", s.K, s.A, s.R)
}

func (s Substance) Decay() Substancer {
    return s
}

func (s Substance) Mutation(...interface{}) Substancer {
    return s
}

func (s Substance) Atsos() []string {
    return []string{string(s.R)}
}

func (s  Substance) Substancer() Substancer {
    return s
}

// func Builder(k Kind, a Action, r Repr) Substance {
//     return Substance{
//         K: k,
//         A: a,
//         R: r,
//     }
// }

func (s Substance) AsNumber() int {
    out, err := strconv.Atoi(string(s.Repr()))
    if err != nil {
        fmt.Printf("Atom: can't convert %v to number\n", s.Repr())
        return 0
    }
    return out
}
