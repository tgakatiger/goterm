// еще одна простая реализация шепелявости
// © translate.google.com

package main

import (
    "fmt"

    "ecrs.ru/goterm/atom"
    "ecrs.ru/goterm/molecule"
    "ecrs.ru/goterm/eval"
    "ecrs.ru/goterm/read"
)

func Help() {
    help := `
Go YASLI v0.0.2
------------------

Yet Another Simply Lisp Imlementation.
`
    fmt.Printf("%s\n", help)
}

func MainLoop() {
    prev_ui := &read.UserInput{}
    cur_ui := &read.UserInput{}
    for prev_ui.String() != "hlt" {
        cur_ui = read.Prompt()
        atoms := atom.Atomizer(cur_ui.Atsos())
        fmt.Printf("Atomizer: %v\n", atoms)
        // eval.Evaluator(*molecule.New(atoms...))
        eval.EvaluationLoop([]molecule.Molecule{*molecule.New(atoms...)})
        prev_ui = cur_ui
    }
}

func main()  {
    Help()
    MainLoop()
}
