package atom

import (
    "fmt"
    "strings"

    "ecrs.ru/goterm/substance"
)

// структура атома
// kind: вид - команда, символ или число
// actn: тип действия в зависимости от вида
// repr: текстовое представление
//type Atom substance.Substancer

type Atom struct {
    substance.Substance
}

func New(k substance.Kind, a substance.Action, r substance.Repr) *Atom {
    return &Atom{substance.New(k, a, r)}
}

func (atom *Atom) Actn(a ...interface{}) substance.Action {
    if a != nil && len(a) > 0 {
        switch t := a[0].(type) {
        case int:
            atom.A = substance.Action(a[0].(int))
        case string:
            // do some if string
        case substance.Action:
            atom.A = a[0].(substance.Action)
        default:
            fmt.Printf("Atom.Actn: Convertion from %T not implemented\n", t)
        }
    }
    return atom.A
}

func (atom *Atom) Kind(k ...interface{}) substance.Kind {
    if k != nil && len(k) > 0 {
        switch t := k[0].(type) {
        case int:
            atom.K = substance.Kind(k[0].(int))
        case string:
            // do some if string
        case substance.Action:
            atom.K = k[0].(substance.Kind)
        case substance.Kind:
            atom.K = k[0].(substance.Kind)
        default:
            fmt.Printf("Atom.Kind: Convertion from %T not implemented\n", t)
        }
    }
    return atom.K
}

func (atom *Atom) Repr(r ...interface{}) substance.Repr {
    if r != nil && len(r) > 0 {
        switch t := r[0].(type) {
        case int:
            atom.R = substance.Repr(r[0].(string))
        case string:
            atom.R = substance.Repr(r[0].(string))
        case substance.Repr:
            atom.R = r[0].(substance.Repr)
        default:
            fmt.Printf("Atom.Repr: Convertion from %T not implemented\n", t)
        }
    }
    return atom.R
}

func (atom *Atom) String() string {
    return fmt.Sprintf("(%d|%d|%s)", atom.K, atom.A, atom.R)
}

func (atom *Atom) Decay() substance.Substancer {
    return atom
}

func (atom *Atom) Mutation(...interface{}) substance.Substancer {
    return atom
}

func (atom *Atom) Atsos() []string {
    return []string{string(atom.R)}
}

// func (atom *Atom) Substancer() substance.Substancer {
//     return atom.Substancer()
// }

// // func Atomizer
// // normal - normalized slice of strings
// // like ui.Prompt returning
func Atomizer(normal []string) []substance.Substancer {
    out := make([]substance.Substancer, 0)
    cp := CommandPool()
    for _, s := range normal {
        a := Atom{}
        if _, ok := cp[s]; ok {
            // is command
            a.Kind(substance.Command)
            a.Actn(cp[s])
            a.Repr(strings.ToUpper(s))
        } else {
            // is symbol or number
            if IsNumber(s) {
                a.Kind(substance.Number)
                a.Actn(substance.Prnt)
                a.Repr(s)
            } else {
                a.Kind(substance.Symbol)
                a.Actn(substance.Prnt)
                a.Repr(s)
            }
        }
        out = append(out, &a)
    }
    return out
}
