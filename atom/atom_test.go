package atom

import (
       "fmt"
       "testing"
	// "strings"
)

// func TestAtomizer(t *testing.T) {
//     tests := map[string][]string{
// 		"1 2 3": strings.Split("1 2 3", " "),
//           "car 1 2 3": strings.Split("car 1 2 3", " "),
//           "cdr 1 2 3": strings.Split("cdr 1 2 3", " "),
//           "hlt": strings.Split("hlt", " "),
//           "cdr 1 hlt 2 3 4": strings.Split("cdr 1 hlt 2 3 4", " "),
//     }

//     for k, v := range tests {
//         tst := Atomizer(strings.Fields(k))
//         got := fmt.Sprintf("%s", tst)
//         exp := fmt.Sprintf("%s", v)
//         if exp != got {
//            t.Errorf("incorrect atomizer: %s\nexpected: %s, got: %s\n", k, exp, got)
//         }
//     }
// }

func TestIsNumberType(t *testing.T) {
    tests := map[string]bool{
        "1": true,
        "0": true,
        "11": true,
        "5/4": true,
        "/5": true,
        "6/": true,
        "-6": true,
        "60-": true,
        "+60": true,
        "-40/70": true,
        "+": true,
        "-": true,
        "-/50": true,
        "0sd": false,
        "/s": false,
        "//": true,
        "/0": true,
        "s/": false,
      }

     for k, v := range tests {
        tst := IsNumberType(k)
        got := fmt.Sprintf("%v", tst)
        exp := fmt.Sprintf("%v", v)
        if exp != got {
           t.Errorf("incorrect number type: %s\nexpected: %s, got: %s\n", k, exp, got)
        }
     }
}

func TestIsNumber(t *testing.T) {
     tests := map[string]bool{
         "1": true,
         "0": true,
         "11": true,
         "5/4": true,
         "/5": false,
         "6/": false,
         "-6": true,
         "60-": false,
         "+60": true,
         "-40/70": true,
         "+": false,
         "-": false,
         "-/50": false,
         "4-2" :false,

      }

     for k, v := range tests {
        tst := IsNumber(k)
        got := fmt.Sprintf("%v", tst)
        exp := fmt.Sprintf("%v", v)
        if exp != got {
           t.Errorf("incorrect is number: %s\nexpected: %s, got: %s\n", k, exp, got)
        }
     }
}
