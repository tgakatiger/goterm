package atom

import (
    "ecrs.ru/goterm/substance"
)

func CommandPool() map[string]substance.Action {
    return map[string]substance.Action {
        "hlt": substance.Hlt,
        "car": substance.Car,
        "cdr": substance.Cdr,
        "lsh": substance.Lsh,
        "rsh": substance.Rsh,
        "knd": substance.Knd,
        "sum": substance.Sum,
        "sub": substance.Sub,
        "div": substance.Div,
        "mul": substance.Mul,
        "com": substance.Com,
        "res": substance.Res,
        "act": substance.Act,
        "rep": substance.Rep,
        "mut": substance.Mut,
        "mol": substance.Mol,
    }
}
