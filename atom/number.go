package atom

type NumberType uint

const (
    Nil NumberType = iota
    One
    Two
    Three
    Four
    Five
    Six
    Seven
    Eight
    Nine
    Divisor
    Sign
)

func NumberPool() map[string]NumberType {
    return map[string]NumberType {
        "0": Nil,
        "1": One,
        "2": Two,
        "3": Three,
        "4": Four,
        "5": Five,
        "6": Six,
        "7": Seven,
        "8": Eight,
        "9": Nine,
        "/": Divisor,
        "-": Sign,
        "+": Sign,
    }
}

func IsNumberType(s string) bool {
    np := NumberPool()
    for _, ch := range s {
        if _, ok := np[string(ch)]; !ok {
            return false
        }
	}
    return true
}

func IsNumber(s string) bool {
	if !IsNumberType(s) {
		return false
	}
	np := NumberPool()
    for i, ch := range s {
		ch_t := np[string(ch)]
		if i == 0 && ch_t == Divisor {
			return false
		}
		if i == (len(s) - 1) && (ch_t == Divisor ||  ch_t == Sign) {
			return false
		}
		if i != 0 && ch_t == Sign {
			return false
		}
		if ch_t == Sign && np[string(s[i+1])] == Divisor {
			return false
		}
	}
  
  return true
}
