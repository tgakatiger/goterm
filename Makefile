test:
	go test ./...

run:
	go run main.go

build:
	go build -o main main.go
	./main

clear:
	rm ./main
